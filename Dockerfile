FROM tensorflow/tensorflow:devel-gpu
LABEL maintainer="Lauro Gomes <laurobmb@gmail.com>"
RUN python -m pip install pip --upgrade
WORKDIR /app
COPY requirements.txt /app
RUN python -m pip install -r requirements.txt 
COPY gpu_vs_cpu.py /app
ENV CUDA_VISIBLE_DEVICES=0,1
CMD [ "python", "gpu_vs_cpu.py" ]