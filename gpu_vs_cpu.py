import tensorflow as tf
import time
import timeit

def prRed(skk): print("\033[91m {}\033[00m" .format(skk))

gpus = tf.config.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu,True)
        logical_gpus = tf.config.experimental.list_physical_devices('GPU')
        print(len(gpus),"Physical GPUs,", len(logical_gpus),"Logical GPUs")
    except RuntimeError as e:
        print(e)
prRed("TP will attempt to allocate only as much GPU memory as needed for the runtime allocations")

###############################################################

def measure(x,steps):
    tf.matmul(x,x)
    start = time.time()
    for i in range(steps):
        x = tf.matmul(x,x)
    _ = x.numpy()
    end = time.time()
    return end - start

shape = (1000,1000)
steps = 100
print("time to multiply a {} matrix by itself {} times:".format(shape,steps))

# run cpu
with tf.device("/cpu:0"):
    cpu_time1 = measure(tf.random.normal(shape),steps)
    print("CPU: {} secs".format(cpu_time1))

# run gpu
if tf.config.list_physical_devices("GPU"):
    with tf.device("/gpu:0"):
        gpu_time1 = measure(tf.random.normal(shape),steps)
        print("GPU: {} secs".format(gpu_time1))
    print("GPU speedup over CPU: {}x".format(int(cpu_time1/gpu_time1)))
else:
    prRed("GPU: not found")

###############################################################

tf.config.run_functions_eagerly(True)

@tf.function
def conv_fn():
    image = tf.random.normal((100,100,100,3))
    net = tf.keras.layers.Conv2D(32,7)(image)
    return tf.reduce_sum(net)

conv_fn()

print("300 loops of :")
with tf.device("/cpu:0"):
    cpu_time2 = timeit.timeit(lambda: conv_fn(),number=30)
    print("CPU:",cpu_time2)
if tf.config.list_physical_devices("GPU"):
    with tf.device("/gpu:0"):
        gpu_time2 = timeit.timeit(lambda: conv_fn(),number=30)
        print("CPU:",gpu_time2)
    print("GPU over speedup over CPU: {}x".format(int(cpu_time2/gpu_time2)))
else:
    prRed("GPU: not found")
