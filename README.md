# CPU_vs_GPU

[![Docker Repository on Quay](https://quay.io/repository/lagomes/cpu_vs_gpu/status "Docker Repository on Quay")](https://quay.io/repository/lagomes/cpu_vs_gpu)

## Build container
	podman build -t gpu:v1 -f Dokerfile

## Run container
	podman run -it --rm --name gpu-vs-cpu gpu:v1

ou	

	podman run -it quay.io/lagomes/cpu_vs_gpu:master


### Fonte: [link](https://www.youtube.com/watch?v=Pm29sQhH3fI)